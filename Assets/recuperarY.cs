﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class recuperarY : MonoBehaviour {

    private float positionY;
    [SerializeField] GameObject bola;

	// Use this for initialization
	void Start () {
        positionY = bola.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider collision) {
        Debug.Log(collision.gameObject.tag);
        if(collision.gameObject.tag == "bola") {
            Debug.Log("Recupera la posición");
            collision.transform.position = new Vector3(transform.position.x, positionY, transform.position.z);
        }
    }
}
