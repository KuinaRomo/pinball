﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class desactivarFuego : MonoBehaviour {

    [SerializeField] GameObject fire;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerExit(Collider other){
        Debug.Log("Entro");
        if(other.tag == "bola") {
            this.gameObject.GetComponent<Collider>().isTrigger = false;
            this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            fire.SetActive(false);
        }
    }
}
