﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class condensadorLights : MonoBehaviour {

    [SerializeField] Material[] lightMaterials;
    int contador;
    int totalChilds;

    // Use this for initialization
    void Start() {
        contador = 0;
        for(int i = 0; i < lightMaterials.Length; i++) {
            lightMaterials[i].DisableKeyword("_EMISSION");
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void encenderLuz() {
        lightMaterials[contador].EnableKeyword("_EMISSION");
        contador++;
    }

    private void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.tag == "bola") {
            if (contador < lightMaterials.Length) {
                encenderLuz();
            }
        }
    }
}
